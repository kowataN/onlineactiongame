﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMove : MonoBehaviour {
	// 重力
	const float kGravityPower = 9.8f;
	// 目的いついたとみなす停止距離
	const float kStoppingDistance = 0.6f;

	// 現在の移動速度
	Vector3 _Velocity = Vector3.zero;
	// キャラクターコントローラーのキャッシュ
	CharacterController _CharacterController;
	// 到着したか
	[SerializeField] private bool _Arrived = false;
	public bool Arrived {
		set { _Arrived = value; }
		get { return _Arrived; }
	}

	// 向きを矯正的に指示するか
	bool _ForceRotate = false;
	// 強制的に向かせたい方向
	Vector3 _ForceRotateDirection;

	// 目的地
	public Vector3 destination;
	// 移動速度
	public float walkSpeed = 6.0f;
	// 回転速度
	public float rotationSpeed = 360.0f;

	// Use this for initialization
	void Start () {
		_CharacterController = GetComponent<CharacterController> ();
		destination = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		// 移動速度を更新
		if (_CharacterController.isGrounded) {
			// 水平面での移動を考えるのXZのみ扱う
			Vector3 destinationXZ = destination;
			destinationXZ.y = transform.position.y; // 高さを現在地と目的地を同じにしておく

			// ここからXZのみで考える

			// 目的地までの距離と方角を求める
			Vector3 direction = (destinationXZ - transform.position).normalized;
			float distance = Vector3.Distance (transform.position, destinationXZ);

			// 現在の速度を退避
			Vector3 currentVelocity = _Velocity;

			// 目的地い近づいたら到着
			if (Arrived || distance < kStoppingDistance) {
				Arrived = true;
			}

			// 移動速度を求める
			_Velocity = (Arrived) ? Vector3.zero : direction * walkSpeed;

			// スムーズに補間
			_Velocity = Vector3.Lerp (currentVelocity, _Velocity, Mathf.Min (Time.deltaTime * 5.0f, 1.0f));
			_Velocity.y = 0;

			if (_ForceRotate == false) {
				// 向きを行きたい方向に向ける
				if (_Velocity.magnitude > 1.0f && Arrived == false) {
					Quaternion characterTargetRotation = Quaternion.LookRotation (direction);
					transform.rotation = Quaternion.RotateTowards (transform.rotation, characterTargetRotation,
						rotationSpeed * Time.deltaTime);
				}
			} else {
				// 強制向き指定
				Quaternion characterTargetRotation = Quaternion.LookRotation (_ForceRotateDirection);
				transform.rotation = Quaternion.RotateTowards (transform.rotation, characterTargetRotation,
					rotationSpeed * Time.deltaTime);
			}
		}

		// 重力
		_Velocity += Vector3.down * kGravityPower * Time.deltaTime;

		// UnityのCharacterControllerの特性のため
		Vector3 snapGround = Vector3.zero;
		if (_CharacterController.isGrounded == true) {
			snapGround = Vector3.down;
		}

		// CharacterControllerを使って動かす
		_CharacterController.Move (_Velocity * Time.deltaTime + snapGround);
		if (_CharacterController.velocity.magnitude < 0.1f) {
			Arrived = true;
		}

		// 強制的に向きを変えるのを解除
		if (_ForceRotate == true &&
		    Vector3.Dot (transform.forward, _ForceRotateDirection) > 0.99f) {
			_ForceRotate = false;
		}
	}
	// 目的地を設定する
	public void SetDestination(Vector3 destination) {
		Arrived = false;
		this.destination = destination;
	}
	// 指定した向きを向かせる
	public void SetDirection(Vector3 direction) {
		_ForceRotateDirection = direction;
		_ForceRotateDirection.y = 0;
		_ForceRotateDirection.Normalize ();
		_ForceRotate = true;
	}
	// 移動をやめる
	public void StopMove() {
		// 現在地を目的地いしてしまう
		destination = transform.position;
	}
}
